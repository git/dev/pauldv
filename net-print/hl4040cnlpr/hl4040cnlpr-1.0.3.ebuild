# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3

inherit base rpm

DESCRIPTION="Cupswrapper driver for Brother HL4040CN"

HOMEPAGE="http://welcome.solutions.brother.com/bsc/public_s/id/linux/en/index.html"
SRC_URI="http://www.brother.com/pub/bsc/linux/dlf/${P}-1.i386.rpm"

LICENSE="as-is"

SLOT="0"

KEYWORDS="~x86 ~amd64"

IUSE=""

# Build-time dependencies, such as
#    ssl? ( >=dev-libs/openssl-0.9.6b )
#    >=dev-lang/perl-5.6.1-r1
# It is advisable to use the >= syntax show above, to reflect what you
# had installed on your system when you tested the package.  Then
# other users hopefully won't be caught without the right version of
# a dependency.

# Run-time dependencies. Must be defined to whatever this depends on to run.
# The below is valid if the same run-time depends are required to compile.
RDEPEND="amd64? ( app-emulation/emul-linux-x86-baselibs )
	    net-print/cups
		>=sys-apps/file-5.06
		app-text/ghostscript-gpl
		app-text/a2ps"

# Source directory; the dir where the sources can be found (automatically
# unpacked) inside ${WORKDIR}.  The default value for S is ${WORKDIR}/${P}
# If you don't need to change it, leave the S= line out of the ebuild
# to keep it tidy.
S="${WORKDIR}/${P}-1"

#AVAILABLE_MODELS="dcp9040cn dcp9042cdn dcp9045cdn hl4040cdn hl4040cn hl4040cnj
#hl4050cdn hl4070cdw mfc9440cn mfc9450cdn mfc9640cw mfc9840cdw"

PATCHES=( "${FILESDIR}/useCupsFileType.patch" )

src_configure() {
# Don't do anything
	sed -i -b -e "s,/usr/local/\(Brother[^\x00]*\),/usr/lib/\1\x00\x00,g" \
		usr/bin/brprintconf_hl4040cn ||die "Relocate resource dir"
	sed -i -b -e "s,/usr/local/Brother,/usr/lib/Brother,g" usr/local/Brother/Printer/hl4040cn/inf/setupPrintcapij
	sed -i -b -e "s,/usr/local/Brother,/usr/lib/Brother,g" usr/local/Brother/Printer/hl4040cn/lpd/filterhl4040cn
}

src_compile() {
	#Compile the config app
	:
}

src_install() {
	dobin usr/bin/brprintconf_hl4040cn ||die "Binary not found"
	
	insinto /usr/lib/Brother/Printer/hl4040cn
	doins -r usr/local/Brother/Printer/hl4040cn/inf ||die "data not found"

	exeinto /usr/lib/Brother/Printer/hl4040cn/lpd
	doexe usr/local/Brother/Printer/hl4040cn/lpd/{brhl4040cnfilter,psconvertij2,filterhl4040cn} ||die "Executables not found"
}
