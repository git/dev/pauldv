# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3

DESCRIPTION="Cupswrapper driver for Brother HL4040CN"

HOMEPAGE="http://welcome.solutions.brother.com/bsc/public_s/id/linux/en/index.html"
SRC_URI="http://www.brother.com/pub/bsc/linux/dlf/${PN}-gpl-src-${PV}-1.tar.gz"

LICENSE="GPL-2"

SLOT="0"

KEYWORDS="~x86 ~amd64"

IUSE=""

# Build-time dependencies, such as
#    ssl? ( >=dev-libs/openssl-0.9.6b )
#    >=dev-lang/perl-5.6.1-r1
# It is advisable to use the >= syntax show above, to reflect what you
# had installed on your system when you tested the package.  Then
# other users hopefully won't be caught without the right version of
# a dependency.
DEPEND=""

# Run-time dependencies. Must be defined to whatever this depends on to run.
# The below is valid if the same run-time depends are required to compile.
RDEPEND="net-print/cups"

PDEPEND=">=net-print/hl4040cnlpr-1.0.3
		app-text/a2ps
		app-text/psutils"

# Source directory; the dir where the sources can be found (automatically
# unpacked) inside ${WORKDIR}.  The default value for S is ${WORKDIR}/${P}
# If you don't need to change it, leave the S= line out of the ebuild
# to keep it tidy.
S="${WORKDIR}/${PN}-gpl-src-${PV}-1"

#AVAILABLE_MODELS="dcp9040cn dcp9042cdn dcp9045cdn hl4040cdn hl4040cn hl4040cnj hl4050cdn hl4070cdw mfc9440cn mfc9450cdn mfc9640cw mfc9840cdw"
AVAILABLE_MODELS="hl4040cn"

src_unpack() {
	unpack ${A}
	cd ${S}
	for f in ${AVAILABLE_MODELS}
	do
		tar xfz ${f}cupswrapper-${PV}-1.tar.gz
	done
	echo ${AVAILABLE_MODELS}| ( read d
		mv ${d}cupswrapper-${PV}-1/brcupsconfig . || die "cupswrapper not found" )
	mkdir ${WORKDIR}/tmp
}

src_configure() {
# Don't do anything
	:
}

src_compile() {
	#Compile the config app
	gcc ${CFLAGS} -o brcupsconfig2 brcupsconfig/brcupsconfig.c || die "Compilation failed"


	#Create the filters
	for m in ${AVAILABLE_MODELS}
	do
		SETUP=${m}cupswrapper-${PV}-1/cupswrapper/cupswrapperSetup_${m}
		ppd_file_name=/usr/share/cups/model/br${m}.ppd

		SETUP2=brlpdwrapper${m}
		SETUP3=brlpdwrapper${m}.tmp

		echo "cat <<MYEOF >$SETUP2" > $SETUP3
		grep -n ENDOFWFILTER $SETUP |cut -d ":" -f 1 | (
			read begin 
			read end
			let end=end-1
			let begin=begin+1
			head -n $end $SETUP | tail -n +$begin \
				| sed -e "s,\${printer_model},$m,g" \
					  -e "s,/usr/local/Brother,/usr/lib/Brother," >>$SETUP3 \
				|| die "Creating filter failed"
		) || die "Creating filter failed"
		echo "MYEOF" >>$SETUP3
		. $SETUP3

		chmod a+x ${SETUP2}

	done
}

src_install() {
	dobin brcupsconfig2 || die "Install failed"

	for m in ${AVAILABLE_MODELS}
	do
		insinto /usr/share/cups/model/
		doins ${m}cupswrapper-${PV}-1/PPD/br${m}.ppd || die "Install failed"

		exeinto /usr/libexec/cups/filter/
		doexe brlpdwrapper${m} || die "Install failed"
	done
}
