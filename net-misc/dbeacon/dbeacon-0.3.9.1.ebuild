# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=2

inherit eutils

DESCRIPTION="dbeacon is a Multicast Beacon written in C++"

HOMEPAGE="http://fivebits.net/proj/dbeacon/"

SRC_URI="http://fivebits.net/files/${PN}/${P}.tar.gz"

LICENSE="GPL-2"

SLOT="0"

KEYWORDS="~amd64"

IUSE=""

RESTRICT="nomirror"

DEPEND=""

RDEPEND="${DEPEND}"

#S="${WORKDIR}/${P}"

src_unpack() {
	unpack "${A}"
	cd "${S}"
	sed -i -e "s,^\([ 	]*CXXFLAGS +=.*\) -g,\1," Makefile
}

src_configure() {
	true # No configure
}

src_compile() {
	emake || die "emake failed"
}

src_install() {
	emake PREFIX="/usr" DESTDIR="${D}" install install_mans || die "emake install failed"
	dodoc README docs/FAQ docs/PROTOCOL docs/large_scale_beaconing.txt
}
