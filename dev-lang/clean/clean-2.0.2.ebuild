# Copyright 1999-2002 Gentoo Technologies, Inc.
# Distributed under the terms of the GNU General Public License v2
# $Header: /home/cvsroot/gentoo-x86/dev-lang/clean/clean-2.0.1.ebuild,v 1.2 2002/08/17 02:27:20 george Exp $

DESCRIPTION="Clean"
HOMEPAGE="http://www.cs.kun.nl/~clean/"
SRC_URI="http://www.cs.kun.nl/~clean/download/Clean20/linux/Clean${PV}.tar.gz http://www.cs.kun.nl/~clean/download/Clean20/source/Clean${PV}Sources.tar.gz"

LICENSE="LGPL-2.1 | clean"
SLOT="0"
KEYWORDS="~x86 -ppc -sparc -sparc64"
RESTRICT="nostrip"
IUSE="X"

DEPEND="virtual/libc
	X? ( x11-libs/xview )"
RDEPEND="${DEPEND}"
S="${WORKDIR}/Clean ${PV} Sources"
S2="${WORKDIR}/clean"

src_unpack() {
	unpack Clean${PV}.tar.gz
	unpack Clean${PV}Sources.tar.gz
	cd "${S}"
	patch -p1 <${FILESDIR}/clean-2.0.1-makeOpt.diff
}

OWHOME=/usr/openwin

src_compile() {
	cd ${S2} ;
	if use X; then
		export OPENWINHOME=${OWHOME}
		make io
	else
		make
	fi

	cd "${S}"
	export PATH=${S2}/bin:${PATH}
	cd CleanTools/clm ; make -f Makefile.linux CFLAGS="${CFLAGS}" || die ; cd -
	export CF="${CFLAGS//\"/} -DI486 -DGNU_C -DLINUX -DLINUX_ELF"
	cd CodeGenerator ; make -f Makefile.linux CFLAGS="${CF}" || die ; cd -
	cd Compiler ; sh make.linux.sh || die ; cd -
	cd Library ; make -f Makefile.linux CFLAGS="${CFLAGS}" ||die ; cd -
}

src_install () {
#	cd "${S}

#	dodir /usr/share/clean/exe
#	exeinto /usr/share/clean/exe
#	doexe Compiler/cocl
#	doexe CodeGenerator/cg
#
#	dodir /usr/bin
#	dobin CleanTools/clm/clm
#
#	dodir /usr/share/clean/iolib
#	insinto /usr/share/clean/iolib
	
#	dodir /usr/share/clean/stdenv
#	insinto /usr/share/clean/stdenv
#	doins

	echo "${S}/codeGenerator/cg"
	cp "${S}/CodeGenerator/cg" ${S2}/exe/cg
	cp "${S}/Library/_startup.o" ${S2}/stdenv/Clean\ System\ Files/_startup.o
	cp "${S}/CleanTools/clm/clm" ${S2}/bin/clm
	cp "${S}/Compiler/cocl" ${S2}/exe/cocl

	cd ${S2}
	make INSTALL_DIR=${D}/usr/share/clean INSTALL_BIN_DIR=${D}/usr/bin INSTALL_MAN_DIR=${D}/usr/share/man
	cp "${S}/CleanTools/clm/clm" ${D}/usr/bin/clm

	./bin/patch_bin ${D}/usr/bin/clm CLEANLIB /usr/share/clean/lib/exe
	./bin/patch_bin ${D}/usr/bin/clm CLEANPATH /usr/share/clean/lib/stdenv:/usr/share/clean/lib/iolib
	
	if use X; then
		ln -sf /usr/share/clean/lib/CleanIDE ${D}/usr/bin/CleanIDE
	else
		rm ${D}/usr/bin/CleanIDE ${D}/usr/share/clean/lib/CleanIDE
	fi

	dodoc "Clean2.0.2LicenseConditions.txt"
}
