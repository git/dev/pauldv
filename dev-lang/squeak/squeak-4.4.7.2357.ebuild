# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-lang/squeak/Attic/squeak-3.10.5.ebuild,v 1.2 2010/04/06 17:36:21 ssuominen dead $

EAPI="4"
inherit base versionator fixheadtails eutils

DESCRIPTION="Highly-portable Smalltalk-80 implementation"
HOMEPAGE="http://www.squeak.org/"
SRC_URI="http://squeakvm.org/unix/release/Squeak-${PV}-src.tar.gz"
LICENSE="Apple"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="X mmx threads iconv opengl"

CMAKE_USE_DIR="unix"

CDEPEND="x11-libs/libX11
		x11-libs/libXext
		x11-libs/libXt
		sys-apps/dbus
		media-libs/freetype:2
		dev-libs/glib:2
		sys-libs/zlib
		sys-apps/util-linux
		dev-libs/libxml2
		media-libs/gstreamer:0.10
		media-libs/alsa-lib
		media-libs/libvorbis
		media-libs/speex
		x11-libs/pango
		x11-libs/cairo
		virtual/libffi
		opengl? ( virtual/opengl )"
DEPEND="${CDEPEND} dev-util/cmake"
RDEPEND="${CDEPEND}"

S="${WORKDIR}/Squeak-${PV}-src"

src_unpack() {
	base_src_unpack
	cd ${S}
	ht_fix_all
	# ht_fix_all doesn't catch this because there's no number
	sed -i -e 's/tail +/tail -n +/' unix/config/inisqueak.in
	mkdir build
	epatch ${FILESDIR}/pluginpath.patch
}

src_configure() {
	myconf="$(use_with opengl gl)
	  --without-vm-display-Quartz
	  --without-vm-sound-Sun
	  --without-vm-sound-NAS
	  --without-vm-sound-MacOSX"
	cd ${S}/build
	ECONF_SOURCE=../unix/cmake econf ${myconf}
}

src_compile() {
	cd ${S}/build
	emake || die
}

src_install() {
	cd ${S}/build
	export DESTDIR="$D" 
	make install ||die
	cd ${D}/usr/lib/squeak/*/
}

#src_install() {
#	cd ${S}/build
#	make ROOT=${D} docdir=/usr/share/doc/${PF} install || die
#	exeinto /usr/lib/squeak
#	doexe inisqueak
#	dosym /usr/lib/squeak/inisqueak /usr/bin/inisqueak
#}
#
#pkg_postinst() {
#	elog "Run 'inisqueak' to get a private copy of the squeak image."
#}
