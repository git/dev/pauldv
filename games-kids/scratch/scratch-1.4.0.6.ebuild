# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

inherit games confutils

DESCRIPTION="A programming environment for creating and sharing interactive stories, animations, games, music, and art."
HOMEPAGE="http://scratch.mit.edu/"
SRC_URI="http://download.scratch.mit.edu/${P}.src.tar.gz"

LICENSE="MIT GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="alsa oss pulseaudio v4l"

DEPEND="
	dev-lang/squeak
	>=x11-libs/cairo-1.8.6
	>=x11-libs/pango-1.20.5
	>=dev-libs/glib-2.20.1:2
	v4l? ( >=media-libs/libv4l-0.5.8 )
"
RDEPEND="${DEPEND}"

S="${WORKDIR}/${P}.src"
ABI="x86"

if   use alsa;       then squeak_sound_plugin="ALSA"
elif use oss;        then squeak_sound_plugin="OSS"
elif use pulseaudio; then squeak_sound_plugin="pulse"
else                      squeak_sound_plugin="null"
fi

pkg_pretend() {
	# Is the a convenience function for "zero or one"?
	confutils_require_one alsa oss pulseaudio
}

src_prepare() {
	if ! use v4l; then
		sed -i '/\/camera/d' "${S}/Makefile"
	fi
	rm Plugins/so.MIDIPlugin Plugins/so.Mpeg3Plugin
#	use alsa       || rm -f Plugins/vm-sound-ALSA
#	use oss        || rm -f Plugins/vm-sound-OSS
#	use pulseaudio || rm -f Plugins/vm-sound-pulse
}

src_compile() {
	cd ${S}
	emake 
}

src_install() {
	local libdir="$(games_get_libdir)/${PN}"
	local datadir="/usr/share/${PN}"
	local icondir="/usr/share/icons/hicolor"
	dodir "${libdir}" "${datadir}"

	insinto "${libdir}"
	doins -r Scratch.* Plugins

	insinto "${datadir}"
	doins -r Help locale Media Projects 

	doman src/man/scratch.1.gz
	dodoc ACKNOWLEDGEMENTS KNOWN-BUGS README LICENSE NOTICE TRADEMARK_POLICY
	
	insinto /usr/share/mime/packages
	echo "3"
	doins src/scratch.xml
	(
		cd src/icons
		for res in *; do
			insinto "${icondir}/${res}/apps"
			doins "${res}"/scratch*.png

			if [ -e "${res}/gnome-mime-application-x-scratch-project.png" ];
			then
				insinto "${icondir}/${res}/mimetypes"
				newins "${res}/gnome-mime-application-x-scratch-project.png" mime-application-x-scratch-project.png
			fi
		done
	)
	install_runner
	make_desktop_entry scratch Scratch scratch "Education;Development" "MimeType=application/x-scratch-project"
}

install_runner() {
	local tmpexe=$(emktemp)
	cat << EOF > "${tmpexe}"
#!/bin/sh
cd
exec /usr/bin/squeak     \\
    -plugins "$(games_get_libdir)/${PN}/Plugins" \\
    -vm-sound-${squeak_sound_plugin}                  \\
	"$(games_get_libdir)/${PN}/Scratch.image"    \\
    "${@}"
EOF
	chmod go+rx "${tmpexe}"
	newbin "${tmpexe}" "${PN}" || die
}
