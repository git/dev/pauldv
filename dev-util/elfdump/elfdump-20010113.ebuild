# Copyright 1999-2002 Gentoo Technologies, Inc.
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="A utility that dumps ELF executable files"
HOMEPAGE="http://www.skyfree.org"
SRC_URI="http://www.skyfree.org/linux/dump_mania/${P}.tgz"
LICENSE=""
SLOT="0"
KEYWORDS="x86"
IUSE=""
DEPEND=""
S="${WORKDIR}/${P}"

src_compile() {
	if [ x${CXX} = x ]; then
	  CXX="g++"
	fi
	${CXX} -o elfdump elfdump.C ||die
}

src_install() {
	dobin elfdump ||die
}
