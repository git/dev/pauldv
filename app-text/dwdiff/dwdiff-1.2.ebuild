# Copyright (C) 2005 G.P. Halkes
# Licensed under the Open Software License version 2.0

SLOT="0"
LICENCSE="OSL-2.0"
KEYWORDS="x86 amd64 ~*"
DESCRIPTION="A delimited word diff program"
SRC_URI="http://os.ghalkes.nl/dist/${P}.tgz"
HOMEPAGE="http://os.ghalkes.nl/dwdiff.html"
RESTRICT="nomirror"
DEPEND="sys-apps/diffutils"
RDEPEND="$DEPEND"
IUSE="nls"

src_compile() {
	LINGUAS=`echo $LINGUAS | grep -o nl`
	./configure --prefix=/usr $( use_with nls gettext )
	make CFLAGS="${CFLAGS}" || die
}

src_install() {
	make prefix="${D}/usr" install || die
}
