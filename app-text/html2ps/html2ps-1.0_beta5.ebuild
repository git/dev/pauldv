# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils perl-app
DESCRIPTION="Convert html into postscript"
HOMEPAGE="http://user.it.uu.se/~jan/html2ps.html"
SRC_URI="http://user.it.uu.se/~jan/${P/_beta/b}.tar.gz"
LICENSE=""
SLOT="0"
KEYWORDS="~x86"
IUSE=""
S=${WORKDIR}/${P/_beta/b}

DEPEND="virtual/ghostscript
	media-gfx/imagemagick
	app-text/tetex
	media-libs/jpeg"

src_compile() {
	sed -i -e "s,^\(\\\$globrc=\)\(.*\$\),\1'/etc/html2psrc';," html2ps
	sed -i -e "s,^\(\\\$ug=\)\(.*\$\),\1'/usr/doc/$P/html2ps.html';," html2ps
	cat <<EOF >html2psrc
/* Global configuration file for html2ps */

@html2ps {
  package {
    ImageMagick: 1;
    PerlMagick: 1;
    djpeg: 1;
    TeX: 1;
    dvips: 1;
    Ghostscript: 1;
    libwww-perl: 1;
    path: "/usr/bin";
  }
  paper {
    type: A4;
  }
  hyphenation {
    en {
      file: "/usr/share/texmf/tex/generic/hyphen/hyphen.tex";
    }
  }
}
EOF
}

src_install() {
	dobin html2ps || die "installation failed"
	doman html2ps.1 html2psrc.5 || die "installation failed"
	dodoc README html2ps.html
	insinto /etc
	doins html2psrc
}
