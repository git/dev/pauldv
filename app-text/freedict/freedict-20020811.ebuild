# Copyright 1999-2002 Gentoo Technologies, Inc.
# Distributed under the terms of the GNU General Public License, v2 or later
# $Header: /home/cvsroot/gentoo-x86/app-text/dictd-dicts/dictd-dicts-1.0.ebuild,v 1.6 2002/08/16 02:42:01 murphy Exp $

DESCRIPTION="A package to simplify installation of my freedict dictionaries"

SLOT="0"
LICENSE="GPL"
KEYWORDS="*"

DEOEND=""

RDEPEND="app-text/freedict-eng-nld
	app-text/freedict-nld-eng
	app-text/freedict-deu-nld
	app-text/freedict-nld-deu"
