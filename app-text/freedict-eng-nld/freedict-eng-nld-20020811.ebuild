# Copyright 1999-2002 Gentoo Technologies, Inc.
# Distributed under the terms of the GNU General Public License, v2 or later
# $Header: $

S=${WORKDIR}
MY_P=${PN/freedict-/}
DESCRIPTION="Freedict english->dutch dictionary"
SRC_URI="http://freedict.sf.net/download/linux/${MY_P}.tar.gz"
HOMEPAGE="http://www.freedict.de"

DEPEND=">=app-text/dictd-1.5.5"

SLOT="0"
LICENSE="GPL"
KEYWORDS="*"

src_install () {
	dodir /usr/lib/dict
	insinto /usr/lib/dict
	doins ${MY_P}.dict.dz
	doins ${MY_P}.index
}
