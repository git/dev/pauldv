# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit eutils git-2 autotools gnome2-utils

DESCRIPTION="A live ebuild for the maynard compositor developed for raspberry pi"

HOMEPAGE="https://github.com/raspberrypi/maynard"

SRC_URI=""

LICENSE="GPL"

SLOT="0"

KEYWORDS="~amd64"

IUSE=""

RESTRICT="nomirror"

DEPEND=">=dev-libs/wayland-1.0.2
		>=dev-libs/weston-1.3.0
		>=x11-libs/gtk+-3.10[wayland]
		>=gnome-base/gnome-menus-3.0
		>=gnome-base/gnome-desktop-3.0
		media-libs/alsa"

RDEPEND="${DEPEND}"

EGIT_REPO_URI="https://github.com/raspberrypi/maynard.git"

#S=${WORKDIR}/${P}

src_prepare() {
	default
	cd $S
	eautoreconf
}

src_install() {
	default
	prune_libtool_files --modules

	# The maynard script is useless and doesn't add anything (and fails because it
	# is development based)
	rm -r ${D}/usr/bin
}

pkg_preinst() {
	gnome2_schemas_savelist
}

pkg_postinst() {
	gnome2_schemas_update
}
