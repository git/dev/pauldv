# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3
REV=929
RESTRICT="nomirror"
MYP="${PN}${PV/./_}"
WANT_ANT_TASKS="ant-nodeps"

inherit java-pkg-2 java-ant-2 eutils

DESCRIPTION="Transforms HTML to well-formed XML"

COMMONDEPEND="
		dev-java/jdom
		"
DEPEND=">=virtual/jdk-1.6
		dev-java/junit:4
		app-arch/unzip
		${COMMONDEPEND}"
RDEPEND=">=virtual/jre-1.6
		${COMMONDEPEND}"

EANT_BUILD_TARGET="jar"


# Homepage, not used by Portage directly but handy for developer reference
HOMEPAGE="http://htmlcleaner.sf.net/"

# Point to any required sources; these will be automatically downloaded by
# Portage.
SRC_URI="mirror://sourceforge/${PN}/${MYP}-all.zip"

LICENSE="as-is"
SLOT="2"
KEYWORDS="~amd64"
IUSE=""

src_unpack() {
	mkdir "${S}"
	cd "${S}"
	unpack ${A}
#	epatch ${FILESDIR}/build.diff
}

src_prepare() {
	cd "${S}/lib"
	rm -v *.jar || die
	java-pkg_jar-from jdom-1.0 jdom.jar jdom.jar
	java-pkg_jar-from ant-core ant.jar
	java-pkg_jar-from junit-4 junit.jar junit-4.4.jar
}

src_install () {
	java-pkg_newjar ${MYP}.jar
#	java-pkg_dolauncher ${PN} \
#		
}
