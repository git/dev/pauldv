# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils
DESCRIPTION="Kinderopvangtoeslag aanvraagprogramma ${PV}"
HOMEPAGE="http://www.toeslagen.nl"
SRC_URI="http://download.belastingdienst.nl/toeslagen/apps/linux/${PN}${PV}_linux.tar.gz"
LICENSE=""
SLOT="$PV"
KEYWORDS="~x86"
RESTRICT="nomirror"

# Build-time dependencies, such as
#    ssl? ( >=dev-libs/openssl-0.9.6b )
#    >=dev-lang/perl-5.6.1-r1
# It is advisable to use the >= syntax show above, to reflect what you
# had installed on your system when you tested the package.  Then
# other users hopefully won't be caught without the right version of
# a dependency.
DEPEND="x11-libs/libXext
	x11-libs/libXdmcp
	x11-libs/libXau"

S=${WORKDIR}/${PN}${PV}

src_compile() {
	echo "binary package"
}

src_install() {
	dobin bin/${PN}${PV}{d,ux}
	insinto /usr/share/belastingdienst.nl/${PN}${PV}/
	doins share/belastingdienst.nl/${PN}${PV}/*
}
