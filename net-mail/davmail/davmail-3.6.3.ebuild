# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3
REV=929
RESTRICT="nomirror"
MYP="${PN}-src-${PV}-${REV}"
WANT_ANT_TASKS="ant-nodeps"

inherit java-pkg-2 java-ant-2 eutils

DESCRIPTION="POP/IMAP/SMTP/CalDav/LDAP Exchange Gateway"

COMMONDEPEND="
        dev-java/ant-nodeps
		dev-java/commons-codec
		dev-java/commons-collections
		dev-java/commons-httpclient
		dev-java/commons-logging
		dev-java/htmlcleaner:2
		dev-java/swt:3.5
		"

DEPEND=">=virtual/jdk-1.6
		${COMMONDEPEND}
		"
RDEPEND=">=virtual/jre-1.6 ${COMMONDEPEND}"

EANT_BUILD_TARGET="jar"


# Homepage, not used by Portage directly but handy for developer reference
HOMEPAGE="http://davmail.sf.net/"

# Point to any required sources; these will be automatically downloaded by
# Portage.
SRC_URI="mirror://sourceforge/${PN}/${MYP}.tgz"

LICENSE="GPL"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

S="${WORKDIR}/${MYP}"

src_unpack() {
	unpack ${A}
	epatch ${FILESDIR}/build.diff
}

src_prepare() {
	cd "${S}/lib"
	rm -v *.jar || die
	java-pkg_jar-from commons-codec commons-collections commons-httpclient \
		commons-logging
	java-pkg_jar-from commons-codec commons-codec.jar lib/commons-codec.jar
}

src_install () {
	java-pkg_dojar dist/davmail.jar
	java-pkg_dolauncher ${PN} \
		
}
