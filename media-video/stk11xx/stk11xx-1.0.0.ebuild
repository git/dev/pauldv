# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

inherit linux-mod

DESCRIPTION="stk-11xx driver for Syntek webcams."
HOMEPAGE="http://syntekdriver.sourceforge.net/"
SRC_URI="mirror://sourceforge/syntekdriver/${P}.tar.gz"
LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""
RESTRICT="nomirror"
DEPEND="dev-util/ctags"
RDEPEND=""

MODULE_NAMES="stk11xx(usb/video:)"
BUILD_TARGETS=" "
CONFIG_CHECK="VIDEO_DEV VIDEO_V4L1_COMPAT"

pkg_setup() {
	linux-mod_pkg_setup
	BUILD_PARAMS="KDIR=${KV_DIR}"
}

src_unpack() {
	unpack ${A}
	epatch "${FILESDIR}"/${P}-ctags.patch
}

src_install() {
	dodoc README
	linux-mod_src_install
}
