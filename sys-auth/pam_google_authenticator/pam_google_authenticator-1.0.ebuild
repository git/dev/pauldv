# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="3"
inherit eutils base pam toolchain-funcs

DESCRIPTION="Example PAM module demonstrating two-factor authentication"
HOMEPAGE="http://code.google.com/p/google-authenticator/"
SRC_URI="https://google-authenticator.googlecode.com/files/libpam-google-authenticator-1.0-source.tar.bz2"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RESTRICT="test nomirror"

DEPEND="sys-libs/pam"
RDEPEND="${DEPEND}
	media-gfx/qrencode"
#PATCHES="${FILESDIR}/exit_codes.patch"

S="${WORKDIR}/lib${PN//_/-}-${PV}"

src_compile() {
	emake CC="$(tc-getCC)" || die
}

src_install() {
	dopammod pam_google_authenticator.so || die

	dobin google-authenticator || die

	dodoc README FILEFORMAT || die
}
