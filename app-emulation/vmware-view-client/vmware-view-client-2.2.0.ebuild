# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $
EAPI=5

inherit eutils
# A well-used example of an eclass function that needs eutils is epatch. If
# your source needs patches applied, it's suggested to put your patch in the
# 'files' directory and use:
#
#   epatch "${FILESDIR}"/patch-name-here
#
# eclasses tend to list descriptions of how to use their functions properly.
# take a look at /usr/portage/eclass/ for more examples.

# Short one-line description of this package.
DESCRIPTION="This is a sample skeleton ebuild file"

HOMEPAGE="https://my.vmware.com/web/vmware/info?slug=desktop_end_user_computing/vmware_horizon_view_clients/2_0"

SRC_URI="http://archive.canonical.com/ubuntu/pool/partner/v/vmware-view-client/${PN}_${PV}.orig.tar.gz"

LICENSE="vmware-view"

SLOT="0"

KEYWORDS="~x86 ~amd64 -*"

IUSE=""

DEPEND="!app-emulation/vmware-view-open-client
		|| ( >=net-misc/freerdp-1.0.2 net-misc/rdesktop )
		x11-libs/libxkbfile[abi_x86_32(-)]"

RDEPEND="x11-libs/libxkbfile[abi_x86_32(-)]"

RESTRICT="strip"

#S=${WORKDIR}/${P}

src_configure() {
	if use amd64; then
		mv ${S}/usr/lib ${S}/usr/lib32 || die "Failure 1"
		LIB32=lib32
	else
		LIB32=lib
	fi
	mkdir -p ${S}/usr/lib/vmware || die "Failure 2"
	if use amd64; then
		mv ${S}/usr/${LIB32}/vmware/xkeymap/ ${S}/usr/lib/vmware/ || die "Moving failed"
	fi
	mkdir -p ${S}/usr/libexec/vmware || die "Failure 4"
	mv ${S}/usr/bin/vmware-view ${S}/usr/bin/vmware-view.bin || die "Failure 5"
	cp ${FILESDIR}/vmware-view.wrapper ${S}/usr/bin/vmware-view || die "Failure 6"
	chmod a+x ${S}/usr/bin/vmware-view || die "Failure 7"
	# Only relocate to other package
}

src_compile() {
	echo -n
	 #do nothing, binary package
}

src_install() {
	cp -dr --preserve=mode,timestamps ${S}/* ${D}/
	insinto /usr/share/pixmaps
	doins ${FILESDIR}/vmware-view-client-vmware.png
	insinto /usr/share/applications
	doins ${FILESDIR}/vmware-view-client.desktop
}
