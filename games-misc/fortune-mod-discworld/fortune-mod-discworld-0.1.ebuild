# Copyright 1999-2002 Gentoo Technologies, Inc.
# Distributed under the terms of the GNU General Public License v2
# $Header: /home/cvsroot/gentoo-x86/app-games/fortune-mod-starwars/fortune-mod-starwars-0.1.ebuild,v 1.2 2002/08/16 00:09:44 bass Exp $

S=${WORKDIR}/${PN/mod-/}
DESCRIPTION="Quotes from the \"Discworld\" novels "
SRC_URI="http://www.splitbrain.org/Fortunes/discworld/fortune-discworld.tgz"
HOMEPAGE="http://www.splitbrain.org/index.php?x=.%2FFortunes%2Fdiscworld"
DEPEND="virtual/glibc"
RDEPEND="app-games/fortune-mod"

SLOT="0"
KEYWORDS="x86 PPC"
LICENSE="GPL-2"

src_install () {
	insinto /usr/share/fortune
	doins discworld discworld.dat
}
