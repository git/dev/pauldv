# Copyright 1999-2005 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-tex/prosper/prosper-1.5-r1.ebuild,v 1.5 2005/04/03 06:03:34 j4rg0n Exp $

inherit latex-package

DESCRIPTION="A latex package including tool for syntax diagrams"
HOMEPAGE="http://gd.tuwien.ac.at/publishing/latex/tex-utils/rail/"
SRC_URI="http://www.adaptivity.nl/${P}.tar.gz"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

DEPEND="dev-util/yacc
        sys-devel/flex"

src_unpack(){
	unpack ${A}
	mkdir ${S}/man/
}

src_compile(){
	emake CFLAGS="-DYYDEBUG ${CFLAGS}" YACC=yacc doc
}

src_install(){
	MANSUFFIX="1"
	cd ${S}
	dobin rail || die "Making rail failed"
	latex-package_src_doinstall styles
	
	if [ ! -d "${D}/usr/share/man/man${MANSUFFIX}" ] ; then
		install -d "${D}/usr/share/man/man${MANSUFFIX}"
	fi

	install -m0644 rail.man "${D}/usr/share/man/man${MANSUFFIX}/rail.${MANSUFFIX}" || die "Installing man page failed"

	gzip -f -9 "${D}/usr/share/man/man${MANSUFFIX}/rail.${MANSUFFIX}" || die "Installing man page failed"

	dodoc README README2e rail.txt rail.dvi || die "Installing docs failed"
}
