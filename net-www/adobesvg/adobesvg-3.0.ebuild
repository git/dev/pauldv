# Copyright 1999-2002 Gentoo Technologies, Inc.
# Distributed under the terms of the GNU General Public License, v2 or later
# $Header: /home/cvsroot/gentoo-x86/app-text/acroread/acroread-5.06.ebuild,v 1.1 2002/07/26 02:43:36 seemant Exp $

S=${WORKDIR}/${P}
DESCRIPTION="Adobe's PDF reader"
SRC_URI="http://download.adobe.com/pub/adobe/magic/svgviewer/linux/3.x/3.0x77/en/adobesvg-3.0-linux-i386.tar.gz"
HOMEPAGE="http://www.adobe.com/products/svgviewer/"

DEPEND="virtual/libc"

SLOT="0"
LICENSE="Adobe"
KEYWORDS="x86"

INSTALLDIR=/usr/lib/svgviewer

src_compile () {
	echo -n ""; # do nothing		
}

src_install () {
	dodir /usr/lib/adobesvg
	cp -p *.so ${D}/usr/lib/adobesvg
	dodoc LICENSE.txt ReadMe.html SVGHelp.html SVGAbout.svg

	#mozilla compatibility contributed by m3thos@netcabo.pt(Miguel Sousa Filipe)
	use mozilla && ( \
		dodir /usr/lib/mozilla/plugins
		dosym \
			/usr/lib/adobesvg/libNPSVG3.so \
				/usr/lib/mozilla/plugins/libNPSVG3.so
	)
}
